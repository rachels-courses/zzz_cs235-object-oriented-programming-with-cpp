#include <iostream>
using namespace std;

void CountUp_Iterative( int start, int end )
{
    for ( int i = start; i <= end; i++ )
    {
        cout << i << "\t";
    }
}

void CountUp_Recursive( int start, int end )
{
    cout << start << "\t";

    // Terminating case
    if ( start == end ) 
        return;
    
    // recursive case
    CountUp_Recursive( start+1, end ); 
}

int main()
{
    cout << "Iterative: " << endl;
    CountUp_Iterative( 1, 10 );

    cout << endl << endl;

    cout << "Recursive: " << endl;
    CountUp_Recursive( 1, 10 );
    
    return 0;
}

#include <iostream>
using namespace std;

int Factorial_Iter( int n )
{
    for ( int i = n-1; i > 0; i-- )
    {
        n *= i;
    }
    return n;
}

int Factorial_Rec( int n )
{
    // Terminating case
    if ( n == 0 )
    {
        return 1;
    }
    
    // Recursive case
    return n * Factorial_Rec( n-1 );
}

int main()
{
    cout << "Iterative: " << endl;
    int fac1 = Factorial_Iter( 5 );
    cout << fac1 << endl;

    cout << endl << endl;

    cout << "Recursive: " << endl;
    int fac2 = Factorial_Rec( 5 );
    cout << fac2 << endl;
    
    return 0;
}

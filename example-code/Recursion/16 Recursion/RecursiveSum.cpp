#include <iostream>
using namespace std;

int SumR( int n )
{
    if ( n == 1 ) { return 1; }
    return n + SumR( n-1 );
}

int main()
{
    cout << SumR( 6 ) << endl;    
    
    return 0;
}

#ifndef _CUSTOMER
#define _CUSTOMER

#include <string>
using namespace std;

struct Customer
{
    int id;
    string name;
    string email;
};

#endif

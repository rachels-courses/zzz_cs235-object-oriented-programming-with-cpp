#include <iostream>
#include <string>
#include <fstream>
#include <map>
using namespace std;

#include "Customer.hpp"

void LoadData( map<int, Customer>& customers );
void SaveData( map<int, Customer>& customers );

int main()
{
    map<int, Customer> customers;

    LoadData( customers );

    // Create customer
    Customer newCustomer;
    cout << "New customer id: ";
    cin >> newCustomer.id;

    cout << "New customer name: ";
    cin.ignore();
    getline( cin, newCustomer.name );

    cout << "New customer email: ";
    getline( cin, newCustomer.email );

    // Add customer to map
    customers[ newCustomer.id ] = newCustomer;

    SaveData( customers );

    return 0;
}

void LoadData( map<int, Customer>& customers )
{
    ifstream input( "Customers.txt" );
    if ( input.fail() )
    {
        // No customers yet
        return;
    }

    Customer newCustomer;

    string buffer;
    while ( getline( input, buffer ) )
    {
        if ( buffer == "NAME" )
        {
            getline( input, newCustomer.name );
        }
        else if ( buffer == "EMAIL" )
        {
            getline( input, newCustomer.email );
        }
        else if ( buffer == "ID" )
        {
            input >> newCustomer.id;
            input.ignore();
        }
        else if ( buffer == "CUSTOMER_END" )
        {
            // Add to the map
            customers[ newCustomer.id ] = newCustomer;
        }
    }

    cout << customers.size() << " customers loaded" << endl;
}

void SaveData( map<int, Customer>& customers )
{
    ofstream output( "Customers.txt" );

    for ( const auto& customer : customers )
    {
        output << "CUSTOMER_BEGIN" << endl;
        output << "NAME" << endl;
        output << customer.second.name << endl;
        output << "EMAIL" << endl;
        output << customer.second.email << endl;
        output << "ID" << endl;
        output << customer.second.id << endl;
        output << "CUSTOMER_END" << endl;
    }
}


#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Employee
{
    string name;
    string address;
    float pay;
};

void AddEmployee( vector<Employee>& employeeList, string name, string address = "UNSET", float pay = 15.00 )
{
    Employee newEmployee;
    newEmployee.name = name;
    newEmployee.address = address;
    newEmployee.pay = pay;
    employeeList.push_back( newEmployee );
}

void DisplayEmployees( const vector<Employee>& employeeList )
{
    for ( unsigned int i = 0; i < employeeList.size(); i++ )
    {
        cout << "EMPLOYEE #" << 0 << ":" << endl;
        cout << "Name:      " << employeeList[i].name << endl;
        cout << "Pay:       " << employeeList[i].pay << endl;
        cout << "Address:   " << employeeList[i].address << endl;
        cout << endl;
    }
}

int main()
{
    vector<Employee> employeeList;

    AddEmployee( employeeList, "Jo Hun-chul" );
    AddEmployee( employeeList, "Rumiko Takahashi", "123 Fake St" );
    AddEmployee( employeeList, "Larry Blamire", "456 Street St", 13.37 );

    DisplayEmployees( employeeList );

    return 0;
}

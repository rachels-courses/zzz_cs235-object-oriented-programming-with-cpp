#include "TheaterProgram.h"

#include "Utilities/Menu.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

void TheaterProgram::Run()
{
    Setup();
    Menu_Login();
    cout << " *** THANK YOU FOR CHOOSING KINEJO THEATERS! ***" << endl;
}

void TheaterProgram::Setup()
{
    m_showings.push_back( Showing( "My Neighbor Totoro", 10 ) );
    m_showings.push_back( Showing( "Coming to America", 15 ) );
    m_showings.push_back( Showing( "Beetle Juice", 12 ) );

    m_tickets.push_back( Ticket( "Child", 3.50 ) );
    m_tickets.push_back( Ticket( "Adult", 5.50 ) );
    m_tickets.push_back( Ticket( "Senior", 4.50 ) );
}

// --------------------------------------------------------------------- //
// ------------------------------- MENUS ------------------------------- //
// --------------------------------------------------------------------- //

void TheaterProgram::Menu_Login()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "LOGIN" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Admin menu",
            "Employee menu"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Menu_AdminMain(); }
        else if ( choice == 2 ) { Menu_EmployeeMain(); }
    }
}

void TheaterProgram::Menu_AdminMain()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN" );
        int choice = Menu::ShowIntMenuWithPrompt( {
            "Set ticket prices",
            "Set auditorium seats",
            "Set movie showings"
        }, true, true );

        if      ( choice == 0 ) { done = true; }
        else if ( choice == 1 ) { Menu_Admin_SetTicketPrices(); }
        else if ( choice == 2 ) { Menu_Admin_SetAudiSeats(); }
        else if ( choice == 3 ) { Menu_Admin_SetMovieShowings(); }
    }
}

void TheaterProgram::Menu_Admin_SetTicketPrices()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN > TICKET PRICES" );

        cout << " 0.\tGo back" << endl;
        DisplayTickets();
        cout << endl;

        int index;
        cout << " >> ";
        cin >> index;

        if ( index == 0 )
        {
            done = true;
        }
        else
        {
            index -= 1;

            int choice = Menu::ShowIntMenuWithPrompt( {
                "Edit name",
                "Edit price"
            } );

            if ( choice == 1 )
            {
                cout << " New name: ";
                cin.ignore();
                getline( cin, m_tickets[index].name );
            }
            else if ( choice == 2 )
            {
                cout << " New price: ";
                cin >> m_tickets[index].price;
            }

            cout << " New ticket info:" << endl << endl << " ";
            m_tickets[index].Display();

            cout << endl << endl;
        }
    }
}

void TheaterProgram::Menu_Admin_SetAudiSeats()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN > AUDITORIUM SEATS" );

        cout << " 0.\tGo back" << endl;
        DisplayShowings();
        cout << endl;

        int index;
        cout << " >> ";
        cin >> index;

        if ( index == 0 )
        {
            done = true;
        }
        else
        {
            index -= 1;

            cout << " New amount of seats: ";
            cin >> m_showings[index].seats;


            cout << " New ticket info:" << endl << " ";
            m_showings[index].Display();

            cout << endl << endl;
        }
    }
}

void TheaterProgram::Menu_Admin_SetMovieShowings()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN > MOVIE SHOWINGS" );

        cout << " 0.\tGo back" << endl;
        DisplayShowings();
        cout << endl;

        int index;
        cout << " >> ";
        cin >> index;

        if ( index == 0 )
        {
            done = true;
        }
        else
        {
            index -= 1;

            cout << " New movie: ";
            cin.ignore();
            getline( cin, m_showings[index].movie );


            cout << " New showing info:" << endl << " ";
            m_showings[index].Display();

            cout << endl << endl;
        }
    }
}


void TheaterProgram::Menu_EmployeeMain()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "EMPLOYEE" );

        cout << " 0.\tGo back" << endl;
        DisplayShowings();
        cout << endl;

        int movieIndex;
        cout << " >> ";
        cin >> movieIndex;

        if ( movieIndex == 0 )
        {
            done = true;
        }
        else
        {
            movieIndex -= 1;

            Menu::DrawHorizontalBar( 80, '.' );
            cout << " MOVIE SELECTED: ";
            m_showings[movieIndex].Display();

            float totalPrice = 0;

            bool ticketSelectionDone = false;
            while ( !ticketSelectionDone )
            {
                cout << endl << endl;
                cout << " TICKET PRICES:" << endl;
                cout << " 0.\tComplete order" << endl;
                DisplayTickets();
                cout << endl << " (Current total: $" << fixed << setprecision( 2 ) << totalPrice << ")" << endl << endl;

                cout << " >> ";
                int ticketIndex;
                cin >> ticketIndex;

                if ( ticketIndex == 0 )
                {
                    ticketSelectionDone = true;
                }
                else
                {
                    ticketIndex -= 1;

                    cout << endl << " TICKET SELECTED: ";
                    m_tickets[ticketIndex].Display();
                    cout << endl << endl;
                    cout << " HOW MANY TICKETS? ";
                    int ticketCount;
                    cin >> ticketCount;

                    // Are there enough seats?
                    if ( ticketCount <= m_showings[movieIndex].seats )
                    {
                        totalPrice += m_tickets[ticketIndex].price * ticketCount;
                        m_showings[movieIndex].seats -= ticketCount;
                    }
                    else
                    {
                        cout << endl << " SORRY, THERE ARE NOT ENOUGH REMAINING SEATS FOR THIS ORDER!" << endl;
                    }
                }
            }

            cout << endl << " THANK YOU FOR ORDERING FROM KINEJO THEATERS!" << endl;
            cout << " YOUR TOTAL IS: $" << fixed << setprecision( 2 ) << totalPrice << endl;
            cout << endl;

        }
    }
}



// --------------------------------------------------------------------- //
// ------------------------------ HELPERS ------------------------------ //
// --------------------------------------------------------------------- //

void TheaterProgram::DisplayShowings()
{
    for ( unsigned int i = 0; i < m_showings.size(); i++ )
    {
        cout << " " << (i+1) << ".\t";
        m_showings[i].Display();
        cout << endl;
    }
}

void TheaterProgram::DisplayTickets()
{
    for ( unsigned int i = 0; i < m_tickets.size(); i++ )
    {
        cout << " " << (i+1) << ".\t";
        m_tickets[i].Display();
        cout << endl;
    }
}

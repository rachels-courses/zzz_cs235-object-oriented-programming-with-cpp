#ifndef _THEATER_STRUCTURES_H
#define _THEATER_STRUCTURES_H

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

struct Showing
{
    Showing( string movie, int seats )
    {
        this->movie = movie;
        this->seats = seats;
    }

    void Display()
    {
        cout << left
            << setw( 30 ) << movie
            << setw( 5 ) << seats
            << "seat(s)";
    }

    string movie;
    int seats;
};

struct Ticket
{
    Ticket( string name, float price )
    {
        this->name = name;
        this->price = price;
    }

    void Display()
    {
        cout << left << fixed << setprecision( 2 )
            << setw( 30 ) << name
            << setw( 1 ) << "$"
            << setw( 30 ) << price;
    }

    string name;
    float price;
};

#endif

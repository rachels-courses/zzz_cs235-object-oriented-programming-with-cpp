#ifndef _THEATER_PROGRAM
#define _THEATER_PROGRAM

#include "TheaterStructures.h"

#include <vector>
using namespace std;

class TheaterProgram
{
    public:
    void Run();

    private:
    void Setup();

    void Menu_Login();
    void Menu_AdminMain();
    void Menu_EmployeeMain();

    void Menu_Admin_SetTicketPrices();
    void Menu_Admin_SetAudiSeats();
    void Menu_Admin_SetMovieShowings();

    void DisplayShowings();
    void DisplayTickets();

    vector<Showing> m_showings;
    vector<Ticket> m_tickets;
};

#endif

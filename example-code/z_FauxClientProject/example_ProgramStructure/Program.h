#ifndef _PROGRAM_H
#define _PROGRAM_H

#include <vector>

#include "Customer.h"
#include "Restaurant.h"
#include "Order.h"

class Program
{
public:
    void Run();

    // MENUS
    void Menu_Login();

    void Menu_Customer();

    void Menu_Admin();
    void Menu_Admin_ViewCustomers();

private:
    void SaveData();
    void LoadData();
    void AddStarterData();

    int m_loggedInId;

    // Program can store all the data needed for the program to run,
    // or you could implement separate managers for each type,
    // if the functionality were complex enough.
    vector<Customer>    m_customers;
    vector<Restaurant>  m_restaurants;
    vector<Order>       m_orders;
};

#endif

#ifndef _SCREEN_DRAWER_HPP
#define _SCREEN_DRAWER_HPP

#include <string>
using namespace std;

class ScreenDrawer
{
public:
    static void Init();

	static void Clear();
	static void Clear(int x, int y);
	static void Set(int x, int y, char symbol);
	static void Set(int x, int y, int number);
	static void Set(int x, int y, string str);

	static void Draw();

	static int GetScreenWidth();
	static int GetScreenHeight();

private:
	static const int SCREEN_WIDTH; // 80
	static const int SCREEN_HEIGHT; // 20

	static bool IsValidRange(int x, int y);
	static void ClearConsole();

	static char s_grid[80][20];
};

#endif

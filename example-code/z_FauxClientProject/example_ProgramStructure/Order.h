#ifndef _ORDER_H
#define _ORDER_H

#include "Restaurant.h"
#include "Customer.h"
#include "Enums.h"

struct Order
{
    int orderId;        // Unique ID for this order
    int customerId;     // Which customer made the order?
    int restaurantId;   // Which restaurant to pick up food from?
    OrderStatus status; // What's the current status of the order?
};

#endif

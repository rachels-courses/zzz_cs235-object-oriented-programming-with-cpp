#ifndef _CUSTOMER_H
#define _CUSTOMER_H

#include <string>
using namespace std;

// This could be a class if you wanted,
// generally more complex objects should be classes.
struct Customer
{
    int id;
    string name;
};

#endif

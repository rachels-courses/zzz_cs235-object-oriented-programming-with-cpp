#ifndef _RESTAURANT_H
#define _RESTAURANT_H

#include <string>
using namespace std;

// This could be a class if you wanted,
// generally more complex objects should be classes.
struct Restaurant
{
    int id;
    string name;
};

#endif

#ifndef _TESTER_HPP
#define _TESTER_HPP

#include <fstream>
using namespace std;

class Tester
{
public:
    void Run();

private:
    void Test_Timeslot();
    void Test_Movie();
    void Test_Auditorium();
    void Test_Showing();

    ofstream m_log;
};

#endif

#include "Auditorium.hpp"

#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Menu.hpp"

#include <iostream>
using namespace std;

Auditorium::Auditorium()
{
    m_name = "NOTSET";
    m_capacity = -1;
}

Auditorium::Auditorium( string name, int capacity )
{
    Setup( name, capacity );
}

void Auditorium::Setup( string name, int capacity )
{
    SetName( name );
    SetCapacity( capacity );
}

void Auditorium::SetName( string name )
{
    m_name = name;
}

void Auditorium::SetCapacity( float capacity )
{
    m_capacity = capacity;
}

string Auditorium::GetName() const
{
    return m_name;
}

int Auditorium::GetCapacity() const
{
    return m_capacity;
}

vector<string> Auditorium::GetFieldNames() const
{
    return { "name", "capacity" };
}

vector<string> Auditorium::ToCsvCells() const
{
    vector<string> cells;
    cells.push_back( m_name );
    cells.push_back( StringUtil::ToString( m_capacity  ) );
    return cells;
}

ostream& operator<<( ostream& out, const Auditorium& item )
{
    out << item.m_name << " (" << item.m_capacity << " seats)";
    return out;
}

istream& operator>>( istream& in, Auditorium& item )
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Edit name",
        "Edit capacity"
    } );

    if ( choice == 1 )
    {
        cout << " New name: ";
        in.ignore();
        string name;
        getline( in, name );
        item.SetName( name );
    }
    else if ( choice == 2 )
    {
        cout << " New capacity: ";
        int capacity;
        in >> capacity;
        item.SetCapacity( capacity );
    }

    return in;
}

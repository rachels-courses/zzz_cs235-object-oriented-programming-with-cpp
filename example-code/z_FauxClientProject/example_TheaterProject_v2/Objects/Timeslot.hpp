#ifndef _TIMESLOT_H
#define _TIMESLOT_H

#include <string>
#include <vector>
using namespace std;

class Timeslot
{
public:
    Timeslot();
    Timeslot( string name, float ticketPrice );

    void Setup( string name, float ticketPrice );

    void SetName( string name );
    void SetTicketPrice( float ticketPrice );

    string GetName() const;
    float GetTicketPrice() const;

    vector<string> GetFieldNames() const;
    vector<string> ToCsvCells() const;
    friend ostream& operator<<( ostream& out, const Timeslot& item );
    friend istream& operator>>( istream& in, Timeslot& item );

private:
    string m_name;
    float m_ticketPrice;

    friend class Tester;
};

#endif

#ifndef _SHOWING_H
#define _SHOWING_H

#include "Timeslot.hpp"
#include "Auditorium.hpp"
#include "Movie.hpp"

struct Showing
{
    void Setup( int movieIndex, int availableSeats );

    int movieIndex;
    int availableSeats;

    friend class Tester;
};

#endif

#include "Timeslot.hpp"

#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Menu.hpp"

#include <iostream>
using namespace std;

Timeslot::Timeslot()
{
    m_name = "UNSET";
    m_ticketPrice = -1;
}

Timeslot::Timeslot( string name, float ticketPrice )
{
    Setup( name, ticketPrice );
}

void Timeslot::Setup( string name, float ticketPrice )
{
    SetName( name );
    SetTicketPrice( ticketPrice );
}

void Timeslot::SetName( string name )
{
    m_name = name;
}

void Timeslot::SetTicketPrice( float ticketPrice )
{
    if ( ticketPrice > 0 ) // Only set if valid price
        m_ticketPrice = ticketPrice;
}

string Timeslot::GetName() const
{
    return m_name;
}

float Timeslot::GetTicketPrice() const
{
    return m_ticketPrice;
}

vector<string> Timeslot::GetFieldNames() const
{
    return { "name", "ticketPrice" };
}

vector<string> Timeslot::ToCsvCells() const
{
    vector<string> cells;
    cells.push_back( m_name );
    cells.push_back( StringUtil::ToString( m_ticketPrice ) );
    return cells;
}

ostream& operator<<( ostream& out, const Timeslot& item )
{
    out << item.m_name << " (" << StringUtil::CurrencyToString( item.m_ticketPrice ) << ")";
    return out;
}

istream& operator>>( istream& in, Timeslot& item )
{
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Edit name",
        "Edit price"
    } );

    if ( choice == 1 )
    {
        cout << " New name: ";
        in.ignore();
        string name;
        getline( in, name );
        item.SetName( name );
    }
    else if ( choice == 2 )
    {
        cout << " New price: ";
        float price;
        in >> price;
        item.SetTicketPrice( price );
    }

    return in;
}




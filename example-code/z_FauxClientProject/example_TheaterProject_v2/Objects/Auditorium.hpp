#ifndef _AUDITORIUM_H
#define _AUDITORIUM_H

#include <string>
#include <vector>
using namespace std;

class Auditorium
{
public:
    Auditorium();
    Auditorium( string name, int capacity );

    void Setup( string name, int capacity );

    void SetName( string name );
    void SetCapacity( float capacity );

    string GetName() const;
    int GetCapacity() const;

    vector<string> GetFieldNames() const;
    vector<string> ToCsvCells() const;
    friend ostream& operator<<( ostream& out, const Auditorium& item );
    friend istream& operator>>( istream& in, Auditorium& item );

private:
    string m_name;
    int m_capacity;

    friend class Tester;
};

#endif

#ifndef _THEATER_PROGRAM
#define _THEATER_PROGRAM

#include "Objects/Auditorium.hpp"
#include "Objects/Movie.hpp"
#include "Objects/Showing.hpp"
#include "Objects/Timeslot.hpp"
#include "Utilities/Logger.hpp"
#include "Utilities/CsvParser.hpp"
#include "Utilities/Menu.hpp"

#include <vector>
#include <sstream>
#include <iostream>
using namespace std;

class TheaterProgram
{
    public:
    TheaterProgram();
    ~TheaterProgram();
    void Run();

    private:
    void Save();
    void Load();
    void Save_Showings();
    void Load_Timeslots();
    void Load_Movies();
    void Load_Auditoriums();
    void Load_Showings();
    void SetDefaults();

    void Menu_Login();
    void Menu_AdminMain();
    void Menu_Customer();

    void Menu_Admin_SetTicketPrices();
    void Menu_Admin_SetAudiSeats();
    void Menu_Admin_SetMovieShowings();
    void Menu_Admin_SetSchedule();
    void Menu_Admin_ViewAll();

    void DisplayShowings();
    string Shorten( string movie, size_t length );
    int GetMovieIndex( string title );

    template <typename T>
    void DisplayNumberedList( vector<T> items, bool offsetByOne = true );

    template <typename T>
    vector<string> VecToString( vector<T> items );

    template <typename T>
    void Menu_Admin_SetData( vector<T> data, string headerText );

    template <typename T>
    void SaveData( vector<T> data, string filepath );

    Showing             m_showings[5][5];
    vector<Timeslot>    m_timeslots;
    vector<Movie>       m_movies;
    vector<Auditorium>  m_auditoriums;

    const string        PATH_TIMESLOTS;
    const string        PATH_MOVIES;
    const string        PATH_AUDITORIUMS;
    const string        PATH_SHOWINGS;
};

template <typename T>
void TheaterProgram::DisplayNumberedList( vector<T> items, bool offsetByOne )
{
    for ( unsigned int i = 0; i < items.size(); i++ )
    {
        if ( offsetByOne )  { cout << i+1; }
        else                { cout << i; }
        cout << ". " << items[i] << endl;
    }
}

template <typename T>
vector<string> TheaterProgram::VecToString( vector<T> items )
{
    vector<string> strItems;

    for ( unsigned int i = 0; i < items.size(); i++ )
    {
        stringstream ss;
        ss << items[i];
        strItems.push_back( ss.str() );
    }

    return strItems;
}

template <typename T>
void TheaterProgram::Menu_Admin_SetData( vector<T> data, string headerText )
{
    Logger::Out( "Function begin", "TheaterProgram::Menu_Admin_SetData" );

    bool done = false;
    while ( !done )
    {
        Menu::Header( "ADMIN > " + headerText );

        int number = Menu::ShowIntMenuWithPrompt( VecToString( data ), true, true );
        int index = number - 1;

        if ( number == 0 ) { done = true; }
        else
        {
            cout << "Selected item: " << data[index] << endl;
            cin >> data[index];
            cout << " Updated info:" << endl << endl << " " << data[index] << endl << endl;
        }
    }
}

template <typename T>
void TheaterProgram::SaveData( vector<T> data, string filepath )
{
    Logger::Out( "Function begin", "TheaterProgram::SaveData" );
    Logger::Out( "Filepath: " + filepath, "TheaterProgram::SaveData" );

    if ( data.size() == 0 ) { return; }

    CsvDocument doc;
    doc.header = data[0].GetFieldNames();
    Logger::Out( "Fields: " + data[0].GetFieldNames()[0] + ", " + data[0].GetFieldNames()[1], "TheaterProgram::SaveData" );

    for ( const auto& item : data )
    {
        doc.rows.push_back( item.ToCsvCells() );
    }

    CsvParser::Save( filepath, doc );
}

#endif

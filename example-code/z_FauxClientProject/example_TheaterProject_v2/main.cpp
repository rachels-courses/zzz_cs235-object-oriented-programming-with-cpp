#include "TheaterProgram.h"
#include "Tests/Tester.hpp"

int main()
{
    Tester tester;
    tester.Run();

    TheaterProgram program;
    program.Run();

    return 0;
}

#ifndef _GAME_PROGRAM_H
#define _GAME_PROGRAM_H

#include "GameStructures.h"

#include <vector>
using namespace std;

class GameProgram
{
    public:
    void Run();

    private:
    Character m_player;
    vector<Character> m_enemies;

    void Setup();

    void Menu_Main();
    void Menu_CharacterCreator();
    void Menu_Battle();

    void Menu_CC_SetName();
    void Menu_CC_SetStats();
    void Menu_CC_SetEquipment();

    void DisplayCharacter( string title, Character character );
    int GetDefenseBonus( string name );
    void Heal( Character& character );
    void Attack( const Character& attacker, Character& defender, int defenderBonus );
};

#endif

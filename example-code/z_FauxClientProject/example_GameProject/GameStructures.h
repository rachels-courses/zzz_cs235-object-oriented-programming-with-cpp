#ifndef _GAME_STRUCTURES_H
#define _GAME_STRUCTURES_H

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

struct Character
{
    Character() {}

    Character( string name, int maxHp, int str, int def, int hitRate, int minDmg, int maxDmg, int ac )
    {
        Setup( name, maxHp, str, def, hitRate, minDmg, maxDmg, ac );
    }

    void Setup( string name, int maxHp, int str, int def, int hitRate, int minDmg, int maxDmg, int ac )
    {
        this->name = name;
        this->maxHp = maxHp;
        this->hp = maxHp;
        this->str = str;
        this->def = def;
        this->hitRate = hitRate;
        this->minDmg = minDmg;
        this->maxDmg = maxDmg;
        this->ac = ac;
    }

    void Display()
    {
        const int SCREEN_WIDTH = 80;
        int totalStats = 6;
        int colwidth = SCREEN_WIDTH / totalStats;

        cout << "~ " << name << " (" << hp << "/" << maxHp << ")" << endl;

        cout << left
             << setw( colwidth ) << "~ STR"
             << setw( colwidth ) << "DEF"
             << setw( colwidth ) << "HIT"
             << setw( colwidth ) << "MINDMG"
             << setw( colwidth ) << "MAXDMG"
             << setw( colwidth ) << "AC"
             << endl;

        cout << left
             << setw( 2 ) << "~ "
             << setw( colwidth-2 ) << str
             << setw( colwidth ) << def
             << setw( colwidth ) << hitRate
             << setw( colwidth ) << minDmg
             << setw( colwidth ) << maxDmg
             << setw( colwidth ) << ac
             << endl;
    }

    void Heal( int amount )
    {
        hp += amount;
        if ( hp > maxHp )
        {
            hp = maxHp;
        }
    }

    int GetDamage() const
    {
        return rand() % ( maxDmg - minDmg + 1 ) + minDmg;
    }

    void TakeDamage( int damage )
    {
        hp -= damage;
    }

    string name;
    int maxHp;
    int hp;

    int str;
    int def;
    int hitRate;
    int minDmg;
    int maxDmg;
    int ac;
};

#endif

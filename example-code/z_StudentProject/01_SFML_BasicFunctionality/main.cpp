#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <iostream>
#include "utilities/StringUtil.hpp"

int main()
{
    // Initialize SFML
    const int SCREEN_WIDTH = 1280;
    const int SCREEN_HEIGHT = 720;
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "My Program");

    // Basic player coordinates
    float x = 492;
    float y = 340;
    float speed = 0.1;

    // Load in textures (image files, usually png)
    bool success;
    sf::Texture textureBackground;
    success = textureBackground.loadFromFile("../assets/graphics/background.png");
    if (!success)
    {
        std::cout << "Error loading image" << std::endl;
        return 1;
    }

    sf::Texture texturePlayer;
    success = texturePlayer.loadFromFile("../assets/graphics/player1.png");
    if (!success)
    {
        std::cout << "Error loading image" << std::endl;
        return 2;
    }

    // Create sprites (multiple sprites can use the same texture, i.e., enemies or blocks.)
    sf::Sprite spriteBackground;
    spriteBackground.setTexture(textureBackground);
    spriteBackground.setPosition(0, 0);

    sf::Sprite spritePlayer;
    spritePlayer.setTexture(texturePlayer);

    // Load in a font
    sf::Font fontMain;
    success = fontMain.loadFromFile("../assets/fonts/RosaSans-Regular.ttf");
    if ( !success )
    {
        std::cout << "Error loading font" << std::endl;
        return 3;
    }

    // Make some text
    sf::Text textPosition;
    textPosition.setFont(fontMain);
    textPosition.setCharacterSize(20);
    textPosition.setFillColor(sf::Color::Black);
    textPosition.setPosition(500, 10);

    // Load sound
    sf::SoundBuffer sbEffect;
    success = sbEffect.loadFromFile("../assets/audio/sfx.wav");
    if ( !success )
    {
        std::cout << "Error loading sound" << std::endl;
        return 4;
    }

    sf::Sound soundEffect;
    soundEffect.setBuffer(sbEffect);

    while (window.isOpen())
    {
        // Check for inputs / events (keyboard, mouse)
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Check if user hits "x" or "alt+f4" or another quit command
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        // Keyboard input
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            x -= speed;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            x += speed;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            y -= speed;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            y += speed;
        }

        // Mouse input
        if ( sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i position = sf::Mouse::getPosition(window);

            // Put player at the mouse position
            x = position.x;
            y = position.y;

            // Play sound
//            soundEffect.play();
        }

        // Update objects
        spritePlayer.setPosition(x, y);

        // Update text
        textPosition.setString("Position: " + StringUtil::ToString(x) + ", " + StringUtil::ToString(y));

        // Update the window (draw)
        window.clear();
        window.draw(spriteBackground);
        window.draw(spritePlayer);
        window.draw(textPosition);
        window.display();
    }

    return 0;
}

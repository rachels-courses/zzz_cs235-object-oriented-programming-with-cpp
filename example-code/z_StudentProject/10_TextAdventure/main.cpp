#include <iostream>
using namespace std;

#include "Program.hpp"
#include "Logger.hpp"

int main()
{
    Logger::Setup( false );
    Program program;
    program.Run();
    Logger::Cleanup();
}

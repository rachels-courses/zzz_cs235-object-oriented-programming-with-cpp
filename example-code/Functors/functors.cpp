#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

template <typename T>
string ToString( const T& value )
{
    stringstream ss;
    ss << value;
    return ss.str();
}

class Logger
{
    public:
    Logger( string file )
    {
        m_output.open( file );
    }

    float operator() ( string data )
    {
        cout << data << endl;
        m_output << data << endl;
    }

    private:
    ofstream m_output;
};

int main()
{
    Logger logger( "programLog.txt" );

    // Example program
    bool done = false;
    int iterations = 0;
    while ( !done )
    {
        logger( "Loop #"  + ToString( iterations ) + " begins" );

        cout << "Enter two numbers: ";
        int a, b;
        cin >> a >> b;

        logger( "User entered numbers: " + ToString( a ) + " and " + ToString( b ) );

        int result = a * b;

        logger( "Result was: " + ToString( result ) );

        cout << endl << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( tolower( choice ) == 'n' )
        {
            logger( "User selected quit" );
            done = true;
        }

        iterations++;
    }

    return 0;
}


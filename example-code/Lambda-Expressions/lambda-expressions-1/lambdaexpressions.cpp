#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
using namespace std;

struct Pet
{
    Pet( string name, string animal, string description, string ownerName, int yearBorn )
    {
        this->name = name;
        this->animal = animal;
        this->description = description;
        this->ownerName = ownerName;
        this->yearBorn = yearBorn;
    }

    string name;
    string animal;
    string description;
    string ownerName;
    int yearBorn;
};

void Initialize( vector<Pet>& pets );
void Display( const vector<Pet>& pets );

int main()
{
    vector<Pet> petList;

    Initialize( petList );

    int currentYear = 2021;

    auto getOldPets = [=]( int olderThanAge, vector<Pet>& rl ) {
        for ( auto pet : petList )
        {
            if ( currentYear - pet.yearBorn >= olderThanAge )
            {
                rl.push_back( pet );
            }
        }
    };

    bool done = false;
    while ( !done )
    {
        cout << endl << endl << "-- Main Menu --" << endl;

        cout << "0. Quit" << endl;
        cout << "1. Display pet list" << endl;
        cout << "2. Filter by animal" << endl;
        cout << "3. Filter by description" << endl;
        cout << "4. Filter by owner" << endl;
        cout << "5. Filter by 'older than'" << endl;
        cout << ">> ";

        int choice;
        cin >> choice;

        switch( choice )
        {
            case 0:
            {
                done = true;
            }
            break;

            case 1:
            {
                cout << endl << "-- Display all --" << endl;
                Display( petList );
            }
            break;

            case 2:
            {
                cout << endl << "-- Filter by animal --" << endl;
                string animal;
                cout << "Animal: ";
                cin >> animal;

                vector<Pet> resultList;

                [&]() {
                    for ( auto pet : petList )
                    {
                        if ( pet.animal == animal )
                        {
                            resultList.push_back( pet );
                        }
                    }
                }();

                Display( resultList );
            }
            break;

            case 3:
            {
                cout << endl << "-- Filter by description --" << endl;
                string description;
                cout << "Description: ";
                cin >> description;

                vector<Pet> resultList;

                []( vector<Pet>& rl, string d, const vector<Pet>& pl ) {
                    for ( auto pet : pl )
                    {
                        if ( pet.description == d )
                        {
                            rl.push_back( pet );
                        }
                    }
                }( resultList, description, petList );

                Display( resultList );
            }
            break;

            case 4:
            {
                cout << endl << "-- Filter by owner --" << endl;
                string owner;
                cout << "Owner: ";
                cin >> owner;

                vector<Pet> resultList = [=]() {
                    vector<Pet> rl;

                    for ( auto pet : petList )
                    {
                        if ( pet.ownerName == owner )
                        {
                            rl.push_back( pet );
                        }
                    }

                    return rl;
                }();

                Display( resultList );
            }
            break;

            case 5:
            {
                cout << endl << "-- Filter by 'older than' --" << endl;
                int age;
                cout << "Age: ";
                cin >> age;

                vector<Pet> resultList;

                getOldPets( age, resultList );

                Display( resultList );
            }
            break;
        }
    }

    return 0;
}

void Initialize( vector<Pet>& pets )
{
    pets.push_back( Pet( "Buddy", "dog",    "black", "Rebekah",     2016 ) );
    pets.push_back( Pet( "Daisy", "dog",    "black", "Rebekah",     2010 ) );
    pets.push_back( Pet( "Roger", "dog",    "white", "Rhonda",      2018 ) );
    pets.push_back( Pet( "Kabe", "cat",     "tan", "Rachel",        2014 ) );
    pets.push_back( Pet( "Luna", "cat",     "black", "Rachel",      2016 ) );
    pets.push_back( Pet( "Pixel", "cat",    "black", "Rachel",      2017 ) );
    pets.push_back( Pet( "Korra", "cat",    "white/gray", "Rachel", 2018 ) );
    pets.push_back( Pet( "Cyrus", "dog",    "white", "Rose",        2004 ) );
}

void Display( const vector<Pet>& pets )
{
    cout << left
        << setw( 15 ) << "NAME"
        << setw( 10 ) << "ANIMAL"
        << setw( 15 ) << "OWNER"
        << setw( 10 ) << "B.YEAR"
        << setw( 20 ) << "DESCRIPTION"
        << endl;
    cout << string( 80, '-' );
    for ( unsigned int i = 0; i < pets.size(); i++ )
    {
        cout << left
            << setw( 15 ) << pets[i].name
            << setw( 10 ) << pets[i].animal
            << setw( 15 ) << pets[i].ownerName
            << setw( 10 ) << pets[i].yearBorn
            << setw( 10 ) << pets[i].description
            << endl;
    }
}


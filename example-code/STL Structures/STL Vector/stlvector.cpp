#include <vector>
#include <array>
#include <string>
#include <iostream>
using namespace std;

int main()
{
    vector<string> students = { "Anuj", "Rai" };

    string name = "";

    while ( true )
    {
        cout << "Enter a student name, or STOP to end: ";
        getline( cin, name );

        if ( name == "STOP" )
        {
            break;
        }

        students.push_back( name );
    }

    cout << endl << "STUDENT NAMES: " << endl;
    for ( auto student : students )
    {
        cout << student << endl;
    }

    cout << students.size() << " total students" << endl;
    cout << students[0] << endl;

    return 0;
}

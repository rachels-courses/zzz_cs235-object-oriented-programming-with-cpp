#include <map>
#include <string>
#include <iostream>
using namespace std;

int main()
{
    map<char, int> countOfLetters;

    string text = "Alice was beginning to get very tired of sitting by her sister on the bank, and of having nothing to do: once or twice she had peeped into the book her sister was reading, but it had no pictures or conversations in it, “and what is the use of a book,” thought Alice “without pictures or conversations?”";

    cout << text << endl;

    for ( auto letter : text )
    {
        try
        {
            // letter is in countOfLetters
            countOfLetters.at( letter );
        }
        catch ( const out_of_range& ex )
        {
            // letter is not already in countOfLetters
            countOfLetters[letter] = 0;
        }

        countOfLetters[letter]++;
    }

    for ( auto cl : countOfLetters )
    {
        cout << cl.first << " = " << cl.second << endl;
    }

    return 0;
}

#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Kitten.hpp"

string GetRandomName();

void Example()
{
    Kitten k;
    cout << "Kitten count: " << Kitten::GetCount() << endl;
}

int main()
{
    // Initialize random number generator
    srand( time( NULL ) );

    // Create a pointer for a dynamic array
    Kitten* pileOfKittens;

    int howMany;
    cout << "How many kittens? ";
    cin >> howMany;

    // Allocate memory
    pileOfKittens = new Kitten[howMany];

    for ( int i = 0; i < howMany; i++ )
    {
        int age = rand() % 20;
        pileOfKittens[i].Setup( GetRandomName(), age );
    }

    Example();

    cout << endl << endl;
    cout << "Total kittens: "
        << Kitten::GetCount()
        << endl << endl;

    cout << left << setw( 10 )
        << "NAME" << setw( 10 ) << "AGE" << endl;
    for ( int i = 0; i < howMany; i++ )
    {
        pileOfKittens[i].Display();
    }

    // Free the memory
    delete [] pileOfKittens;

    return 0;
}

string GetRandomName()
{
    vector<char> vowels = { 'a', 'e', 'i', 'o', 'u', 'y' };
    vector<char> consonants = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z' };
    string name = "";

    int nameLength = rand() % 4 + 3;

    for ( int i = 0; i < nameLength; i++ )
    {
        if ( i % 2 == 0 )
        {
            int index = rand() % consonants.size();
            name += consonants[ index ];
        }
        else
        {
            int index = rand() % vowels.size();
            name += vowels[ index ];
        }
    }

    return name;
}

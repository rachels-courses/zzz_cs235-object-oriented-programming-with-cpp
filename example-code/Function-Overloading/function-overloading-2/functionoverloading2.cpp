#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

void Display( string data )
{
    cout << "\"" << data << "\"" << endl;
}

void Display( char data )
{
    cout << "'" << data << "'" << endl;
}

void Display( float data )
{
    cout << "$" << fixed << setprecision(2) << data << endl;
}

void Display( int data )
{
    cout << "#" << data << endl;
}

int main()
{
    string productName = "Textbook";
    char symbol = '$';
    float price = 12345.40;
    int quantity = 5;

    Display( productName );
    Display( symbol );
    Display( price );
    Display( quantity );

    return 0;
}

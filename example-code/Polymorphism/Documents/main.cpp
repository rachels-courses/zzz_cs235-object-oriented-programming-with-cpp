#include "Document.hpp"
#include "Menu.hpp"

void AutomatedProgram();
void InteractiveProgram();

int main()
{
    Menu::Header( "CHOOSE DOCUMENT TYPE" );

    Document* myDoc = nullptr;

    string type = Menu::ShowStringMenuWithPrompt( {
        "Text document",
        "CSV document",
        "HTML document"
    } );

    if ( type == "Text document" )
    {
        myDoc = new TextDocument;
    }
    else if ( type == "CSV document" )
    {
        myDoc = new CsvDocument;
    }
    else if ( type == "HTML document" )
    {
        myDoc = new HtmlDocument;
    }

    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU - " + myDoc->GetType() );
        string choice = Menu::ShowStringMenuWithPrompt( {
            "Add",
            "Save",
            "View",
            "Quit"
        } );

        if ( choice == "Add" )
        {
            myDoc->Add();
        }
        else if ( choice == "Save" )
        {
            string filename;
            cout << "Enter filename (no extension): ";
            cin >> filename;
            myDoc->Save( filename );
        }
        else if ( choice == "View" )
        {
            myDoc->View();
        }
        else if ( choice == "Quit" )
        {
            done = true;
        }
    }

    delete myDoc;
    return 0;
}

#ifndef _QUESTIONS_HPP
#define _QUESTIONS_HPP

#include <string>
#include <iostream>
using namespace std;

class Question
{
    public:
    Question();
    ~Question();

    bool AskQuestion();
    void DisplayQuestion();

    protected:
    string m_question;
};

class TrueFalseQuestion : public Question
{
    public:
    TrueFalseQuestion();
    ~TrueFalseQuestion();

    bool AskQuestion();

    protected:
    bool m_answer;
};

class MultipleChoiceQuestion : public Question
{
    public:
    MultipleChoiceQuestion();
    ~MultipleChoiceQuestion();

    bool AskQuestion();
    void ListAllAnswers();

    protected:
    string m_options[4];
    int m_correct;
};

class FillInQuestion : public Question
{
    public:
    FillInQuestion();
    ~FillInQuestion();

    bool AskQuestion();

    protected:
    string m_answer;
};

#endif

#include <iostream>
#include <string>
using namespace std;

#include "Questions.hpp"
#include "Menu.hpp"

int main()
{
    Question* ptrQuestion = nullptr;

    // ----------------------------------------- //
    // - EXAMPLE 1: TRUE/FALSE QUESTION        - //
    // ----------------------------------------- //
    Menu::Header( "EXAMPLE 1: TRUE/FALSE QUESTION" );

    ptrQuestion = new TrueFalseQuestion;

    ptrQuestion->AskQuestion();

    delete ptrQuestion;

    cout << endl;
    // ----------------------------------------- //
    // - EXAMPLE 2: FILL-IN-THE-BLANK QUESTION - //
    // ----------------------------------------- //
    Menu::Header( "EXAMPLE 2: FILL-IN-THE-BLANK QUESTION" );

    ptrQuestion = new FillInQuestion;

    ptrQuestion->AskQuestion();

    delete ptrQuestion;

    cout << endl;
    // ----------------------------------------- //
    // - EXAMPLE 3: MULTIPLE CHOICE QUESTION   - //
    // ----------------------------------------- //
    Menu::Header( "EXAMPLE 3: MULTIPLE CHOICE QUESTION" );

    ptrQuestion = new MultipleChoiceQuestion;

    ptrQuestion->AskQuestion();

    delete ptrQuestion;

    cout << endl;

    // ------------------------------------------------------------------------- //
    // - EXAMPLE 4: MULTIPLE CHOICE QUESTION (directly call unique function)   - //
    // ------------------------------------------------------------------------- //
    Menu::Header( "EXAMPLE 4: MULTIPLE CHOICE QUESTION (directly call unique function)" );

    ptrQuestion = new MultipleChoiceQuestion;

    // This won't work:
    //ptrQuestion->ListAllAnswers();

    // Gotta use a cast:
    static_cast<MultipleChoiceQuestion*>(ptrQuestion)->ListAllAnswers();

    delete ptrQuestion;

    cout << endl;


    return 0;
}

#ifndef _QUESTIONS_HPP
#define _QUESTIONS_HPP

#include <string>
#include <iostream>
using namespace std;

class Question
{
    public:
    Question();
    virtual ~Question();

    virtual bool AskQuestion() = 0;
    virtual void DisplayQuestion();

    protected:
    string m_question;
};

class TrueFalseQuestion : public Question
{
    public:
    TrueFalseQuestion();
    virtual ~TrueFalseQuestion();

    virtual bool AskQuestion();

    protected:
    bool m_answer;
};

class MultipleChoiceQuestion : public Question
{
    public:
    MultipleChoiceQuestion();
    virtual ~MultipleChoiceQuestion();

    virtual bool AskQuestion();
    virtual void ListAllAnswers();

    protected:
    string m_options[4];
    int m_correct;
};

class FillInQuestion : public Question
{
    public:
    FillInQuestion();
    virtual ~FillInQuestion();

    virtual bool AskQuestion();

    protected:
    string m_answer;
};

#endif
